import React, { Fragment, useState, useEffect } from 'react';

// Components
import Form from './components/Form';
import Quote from './components/Quote';

function App() {
  // Quotes in LocalStorage
  let initialQuotes = JSON.parse(localStorage.getItem('quotes'));
  if (!initialQuotes) {
    initialQuotes = [];
  }

  //Array Quotes
  const [quotes, setQuotes] = useState(initialQuotes);

  // UseEffect
  useEffect(() => {}, [quotes]);
  if (initialQuotes) {
    localStorage.setItem('quotes', JSON.stringify(quotes));
  } else {
    localStorage.setItem('quotes', JSON.stringify([]));
  }
  // Read Quote and Write in the state
  const createQuote = (quote) => {
    setQuotes([...quotes, quote]);
  };

  //

  const deleteQuote = (idQuote) => {
    const newQuotes = quotes.filter((quote) => quote.id !== idQuote);

    setQuotes(newQuotes);
  };

  const title = quotes.length === 0 ? 'No hay citas' : 'Administra tus citas';

  return (
    <Fragment>
      <h1>Administrador de Pacientes</h1>

      <div className="container">
        <div className="row">
          <div className="one-half column">
            <Form createQuote={createQuote} />
          </div>
          <div className="one-half column">
            <h2>{title}</h2>

            {quotes.map((quote) => (
              <Quote key={quote.id} quote={quote} deleteQuote={deleteQuote} />
            ))}
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
