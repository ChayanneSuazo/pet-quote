import React, { Fragment, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

import PropTypes from 'prop-types';

const Form = ({ createQuote }) => {
  // Created State of Quote
  const [quote, setQuote] = useState({
    pet: '',
    owner: '',
    date: '',
    time: '',
    symptoms: '',
  });

  // Created State of Error
  const [error, setError] = useState(false);

  // Function to change State
  const handleChange = (e) => {
    setQuote({
      ...quote,
      [e.target.name]: e.target.value,
    });
  };

  // Extract Values

  const { pet, owner, date, time, symptoms } = quote;

  // ON Submit

  const submitQuote = (e) => {
    e.preventDefault();

    //Validate Form
    if (
      pet.trim() === '' ||
      owner.trim() === '' ||
      date.trim() === '' ||
      time.trim() === '' ||
      symptoms.trim() === ''
    ) {
      setError(true);
      return;
    }

    // Assign ID
    quote.id = uuidv4();

    // Create Quote
    createQuote(quote);

    // Reset Form and Delete Message Error
    setError(false);
    setQuote({
      pet: '',
      owner: '',
      date: '',
      time: '',
      symptoms: '',
    });
  };

  return (
    <Fragment>
      <h2> Crear Cita </h2>

      {error ? (
        <p className="alerta-error">Todos los campos son obligatorios</p>
      ) : null}
      <form onSubmit={submitQuote}>
        <label>Nombre Mascota</label>
        <input
          type="text"
          name="pet"
          className="u-full-width"
          placeholder="Nombre Mascota"
          onChange={handleChange}
          value={pet}
        />
        <label>Nombre del Dueño</label>
        <input
          type="text"
          name="owner"
          className="u-full-width"
          placeholder="Nombre del dueño de la mascota"
          onChange={handleChange}
          value={owner}
        />
        <label>Fecha de salida</label>
        <input
          type="date"
          name="date"
          className="u-full-width"
          onChange={handleChange}
          value={date}
        />
        <label> Hora de salida </label>
        <input
          type="time"
          name="time"
          className="u-full-width"
          onChange={handleChange}
          value={time}
        />
        <label>Sintomas</label>
        <textarea
          className="u-full-width"
          name="symptoms"
          onChange={handleChange}
          value={symptoms}
        ></textarea>

        <button type="submit" className="u-full-width button-primary">
          Agregar Cita
        </button>
      </form>
    </Fragment>
  );
};

Form.propTypes = {
  createQuote: PropTypes.func.isRequired,
};

export default Form;
